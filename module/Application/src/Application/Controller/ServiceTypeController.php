<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Entity\Servicetype;

class ServiceTypeController extends AbstractRestfulController {
    
    /**
     * Restfull endpoint to list Service types
     * @return JsonModel
     */
    public function getList() {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $repo = $em->getRepository('\Application\Entity\Servicetype');
        $rs = $repo->findAll();

        $finalRs = array();
        foreach ($rs as $row) {
            $finalRs[] = array(
                'id' => $row->getId(),
                'name' => $row->getName(),
                'description' => $row->getDescription()
            );
        }

        return new JsonModel(array(
            'data' => $finalRs
        ));
    }
    
    /**
     * Restfull endpoint to get details of one service type
     * @return JsonModel
     */
    public function get($id) {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
        
        $rs = $em->find('\Application\Entity\Servicetype', $id);

        return new JsonModel(array(
            'data' => array(
                'id' => $rs->getId(),
                'name' => $rs->getName(),
                'description' => $rs->getDescription()                
            )
        ));
    }
    
    /**
     * Restfull endpoint to create a new service type
     * @param type $data
     * @return JsonModel
     */
    public function create($data) {
        
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
               
        $model = new Servicetype();
        
        $model->setName($data['name']);
        $model->setDescription($data['description']);    
        
        $em->persist($model);
        $em->flush();
        
        return new JsonModel(array(
            'data' => array(
                'id' => $model->getId(),
                'name' => $model->getName(),
                'description' => $model->getDescription()                
            )
        ));
    }
    
    /**
     * Restfull endpoint to edit a existing service type
     * @param type $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data) {
//        var_dump($data);
//        die;
//        $data = $data['data'];
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
        
        $model = $em->find('\Application\Entity\Servicetype', $id);
        
        if($model && isset($data['name']) && $data['name'])
            $model->setName($data['name']);
        
        if($model && isset($data['description']) && $data['description'])
            $model->setDescription($data['description']);    
        
        $em->persist($model);
        $em->flush();
        
        return new JsonModel(array(
            'data' => array(
                'id' => $model->getId(),
                'name' => $model->getName(),
                'description' => $model->getDescription()                
            )
        ));
    }
    
    /**
     * Restfull endpoint to remove a service type
     * @param type $id
     * @return JsonModel
     */
    public function delete($id) {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
        
        $rs = $em->find('\Application\Entity\Servicetype', $id);
        $em->remove($rs);
        $em->flush();

        return new JsonModel(array(
            'data' => 'deleted',
        ));
    }


}
