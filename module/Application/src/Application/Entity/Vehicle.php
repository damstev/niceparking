<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehicle
 *
 * @ORM\Table(name="Vehicle", uniqueConstraints={@ORM\UniqueConstraint(name="plate_UNIQUE", columns={"plate"})}, indexes={@ORM\Index(name="fk_Vehicle_Client1_idx", columns={"Client_id"}), @ORM\Index(name="fk_Vehicle_ServiceType1_idx", columns={"ServiceType_id"}), @ORM\Index(name="fk_Vehicle_VehicleStatus1_idx", columns={"VehicleStatus_id"})})
 * @ORM\Entity
 */
class Vehicle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="plate", type="string", length=45, nullable=false)
     */
    private $plate;

    /**
     * @var string
     *
     * @ORM\Column(name="branch", type="string", length=45, nullable=true)
     */
    private $branch;

    /**
     * @var string
     *
     * @ORM\Column(name="line", type="string", length=45, nullable=true)
     */
    private $line;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=45, nullable=true)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=45, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=45, nullable=true)
     */
    private $class;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="application_date", type="date", nullable=true)
     */
    private $applicationDate;

    /**
     * @var \Application\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \Application\Entity\Servicetype
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Servicetype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ServiceType_id", referencedColumnName="id")
     * })
     */
    private $servicetype;

    /**
     * @var \Application\Entity\Vehiclestatus
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Vehiclestatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="VehicleStatus_id", referencedColumnName="id")
     * })
     */
    private $vehiclestatus;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set plate
     *
     * @param string $plate
     *
     * @return Vehicle
     */
    public function setPlate($plate)
    {
        $this->plate = $plate;

        return $this;
    }

    /**
     * Get plate
     *
     * @return string
     */
    public function getPlate()
    {
        return $this->plate;
    }

    /**
     * Set branch
     *
     * @param string $branch
     *
     * @return Vehicle
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return string
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * Set line
     *
     * @param string $line
     *
     * @return Vehicle
     */
    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * Get line
     *
     * @return string
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Vehicle
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Vehicle
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set class
     *
     * @param string $class
     *
     * @return Vehicle
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set applicationDate
     *
     * @param \DateTime $applicationDate
     *
     * @return Vehicle
     */
    public function setApplicationDate($applicationDate)
    {
        $this->applicationDate = $applicationDate;

        return $this;
    }

    /**
     * Get applicationDate
     *
     * @return \DateTime
     */
    public function getApplicationDate()
    {
        return $this->applicationDate;
    }

    /**
     * Set client
     *
     * @param \Application\Entity\Client $client
     *
     * @return Vehicle
     */
    public function setClient(\Application\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Application\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set servicetype
     *
     * @param \Application\Entity\Servicetype $servicetype
     *
     * @return Vehicle
     */
    public function setServicetype(\Application\Entity\Servicetype $servicetype = null)
    {
        $this->servicetype = $servicetype;

        return $this;
    }

    /**
     * Get servicetype
     *
     * @return \Application\Entity\Servicetype
     */
    public function getServicetype()
    {
        return $this->servicetype;
    }

    /**
     * Set vehiclestatus
     *
     * @param \Application\Entity\Vehiclestatus $vehiclestatus
     *
     * @return Vehicle
     */
    public function setVehiclestatus(\Application\Entity\Vehiclestatus $vehiclestatus = null)
    {
        $this->vehiclestatus = $vehiclestatus;

        return $this;
    }

    /**
     * Get vehiclestatus
     *
     * @return \Application\Entity\Vehiclestatus
     */
    public function getVehiclestatus()
    {
        return $this->vehiclestatus;
    }
}
